import 'package:demo_comment/dynamics_color.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:comment/module_comment.dart';
import 'package:models/module_models.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  initCommentService();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => CommentModel(
            AppSetting(
              userId: 'uid',
              userName: 'me',
            ),
          ),
        ),
        ChangeNotifierProvider(
            create: (_) => AppSettingModel()..getAppSetting()),
      ],
      child: DynamicsColor(
        value: appColor,
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: appColor.themeData,
          home: MyHomePage(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: OutlinedButton(
          child: Text('Go to comment screen'),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => CommentScreen(
                  topic: Topic(
                    id: '1',
                    title: 'title',
                    parentId: 'parentId',
                    questionNum: 1,
                    indexs: 1,
                    practiceMode: 1,
                    shortDes: 'shortDes',
                    topicType: TopicType.normal,
                    image: 'image',
                    type: 1,
                    mode: 1,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

initCommentService() {
  CommentService commentService = CommentServiceFirebaseDefaultImpl(
    androidAppId: '1:476712289763:android:b21b085bd18035ad01ccd6',
    iosAppId: '1:476712289763:ios:b21b085bd18035ad01ccd6',
    apiKey: 'AIzaSyBRNTwxSNDQ8KpkdmfLsbI2WPveqPdcUPI',
    messagingSenderId: 'messagingSenderId',
    projectId: 'democomment-3df1c',
  );
  CommentServiceInitializer().init(commentService);
}
